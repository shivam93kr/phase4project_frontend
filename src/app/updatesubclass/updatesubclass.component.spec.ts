import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatesubclassComponent } from './updatesubclass.component';

describe('UpdatesubclassComponent', () => {
  let component: UpdatesubclassComponent;
  let fixture: ComponentFixture<UpdatesubclassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatesubclassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatesubclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
