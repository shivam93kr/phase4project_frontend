import { Component, OnInit } from '@angular/core';
import { Classes } from '../classes';
import { Subject } from '../subject';
import { ClassService } from '../class.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updatesubclass',
  templateUrl: './updatesubclass.component.html',
  styleUrls: ['./updatesubclass.component.css']
})
export class UpdatesubclassComponent implements OnInit {

  public C:Classes;
  public class:Classes[]=[];

  public subject: Subject;
  public subjectList: Subject[]=[];
  constructor(private service:ClassService,private route:Router) {
    this.C=new Classes();
    
    this.subject = new Subject();
   }

   public updatesubclass(){
    this.mergeSubjects();
    return this.service.updatesubClass(this.C).subscribe(
      data=>{this.C=new Classes();
       
        this.subject=new Subject();
  
    }
    );
   }


  mergeSubjects(){
    var merge = this.class.filter(data=>{
      return data.classId == this.C.classId})
  
        this.C.subjectList=merge[0].subjectList;
        console.log(this.C.subjectList);
  if(this.subject.subjectId){           
      console.log(merge[0].subjectList);
      merge[0].subjectList.push(this.subject)
      this.C.subjectList=merge[0].subjectList;
  }
  
  }

  ngOnInit() {
    this.service.getAllClass().subscribe(data=>{
      this.class=data;
      //console.log(this.class);
      
    });
  }

}
